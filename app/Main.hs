
{-# LANGUAGE OverloadedStrings #-}
module Main (main) where

import Debug.Trace (trace)

import Control.Monad (when, unless, forM_)
import Data.Text (Text)
import Foreign.C.Types (CInt)
import Linear
import SDL (($=))
import qualified SDL

import Graphics.Rendering.OpenGL (GLfloat)
import qualified Graphics.Rendering.OpenGL as GL

myPoints :: [(GLfloat,GLfloat,GLfloat)]
myPoints = [ (sin (2*pi*k/12), cos (2*pi*k/12), 0) | k <- [1..12] ]

makeWindow :: Text -> CInt -> CInt -> Bool -> IO SDL.Window
makeWindow title width height resizeable =
  SDL.createWindow title
                   SDL.defaultWindow
                   {SDL.windowInitialSize = V2 width height
                   ,SDL.windowGraphicsContext = SDL.OpenGLContext SDL.defaultOpenGL
                   ,SDL.windowResizable = resizeable
                   }

setVideoHints :: Bool -> IO ()
setVideoHints vsync = do
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do renderQuality <- SDL.get SDL.HintRenderScaleQuality
     when (renderQuality /= SDL.ScaleLinear) $
       putStrLn "Warning: Linear texture filtering not enabled!"
  SDL.HintRenderVSync $= if vsync then SDL.EnableVSync else SDL.DisableVSync

gameLoop :: SDL.Window -> IO ()
gameLoop window = do
  let loop = do
        events <- SDL.pollEvents
        -- debug events:
        -- when (events /= []) $
        --   putStrLn $ show events
          
        let quit = any (== SDL.QuitEvent) $ map SDL.eventPayload events

        GL.clear [GL.ColorBuffer]
        GL.renderPrimitive GL.Points $
           mapM_ (\(x, y, z) -> GL.vertex $ GL.Vertex3 x y z) myPoints
        GL.flush
        SDL.glSwapWindow window

        unless quit (loop)

  loop

startGame :: Text -> CInt -> CInt -> Bool -> (SDL.Window -> IO ()) -> IO ()
startGame title width height resizeable gloop = do
  window <- makeWindow "Haskell OpenGL Lea(r)n tutorial"
            800 600 True

  setVideoHints True  -- enable VSync and Linear scaling rendering
            
  SDL.showWindow window

  SDL.glCreateContext(window)

  gloop window

  SDL.destroyWindow window

main :: IO ()
main = do
  SDL.initializeAll -- [SDL.InitVideo]
  startGame "Haskell OpenGL Lea(r)n tutorial" 800 600 True gameLoop
  SDL.quit
